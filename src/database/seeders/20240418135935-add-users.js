'use strict';
const { v4: uuidv4 } = require('uuid');
const { faker } = require('@faker-js/faker');

let hashedPassword =
  '$2b$10$x2aZ9HUwCRpjWENSvXFQuOLYR8FVG6bTQ62LFm61Ehm1OYjr93iZy';

let users = '';

for (var i = 1; i <= 2; i++) {
  users += `('${uuidv4()}', '${faker.name.firstName().replace("'", '')}','${faker.internet.email().replace("'", '')}', '${hashedPassword}','2023-05-06 15:33:07','2023-05-06 15:33:07'),`;
}
users += `('${uuidv4()}', 'Mehdi' ,'mehdi@gmail.com', '${hashedPassword}','2023-05-06 15:33:07','2023-05-06 15:33:07');`;

module.exports = {
  async up(queryInterface, Sequelize) {
    const query = `
      INSERT INTO user
      (id, first_name, email, password, created_at, updated_at)
      VALUES
      ${users}`;

    return queryInterface.sequelize.query(query);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.sequelize.query(`
    DELETE from user WHERE 1=1;
    `);
  },
};
